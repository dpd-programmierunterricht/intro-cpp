/// FOR
///
/// Mit einer For-Schleife kannst du ein Codesegment mehrmals ausführen.
/// Dies nutzt man meistens, wenn man ein Segment eine bekannte Anzahl an
/// Wiederholungen ausführen will.
///
/// Aufgabe:
///
/// 1. Implementiere die `summer()` Funktion so, dass diese immer die Summe
///    aller Zahlen von 0 bis maximal addiert.
///
/// Tipp: mit "x = x + y" kannst du den Wert von y auf x addieren.

#include <cassert>
#include <iostream>

// Berechne die Summe von allen Zahlen von 0 bis maximal.
int summe(int maximal) {
    int s = 0;

    for(int i = 0; i <= maximal; i++) {
        std::cout << "I ist " << i << "." << std::endl;

        // TODO: addiere die Zählervariable i auf s.
    }

    return s;
}


int main() {
    assert(summe(0) == 0);
    assert(summe(1) == 1);
    assert(summe(2) == 3);
    assert(summe(3) == 6);
    assert(summe(4) == 10);
    assert(summe(5) == 15);
    assert(summe(6) == 21);
}
