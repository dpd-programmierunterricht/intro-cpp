# Flow

In der Programmierung ist *Flow* (Control flow) der "Fluss" des Programmes durch
den Code. Dieser wird mit Konstrukten wie `if`-Abfragen, `for`- und `while`-Schleifen
gesteuert.

Nützliche Links:

- [C++ Referenz: If](https://de.cppreference.com/w/cpp/language/if)
- [C++ Referenz: Switch](https://de.cppreference.com/w/cpp/language/switch)
- [C++ Referenz: For](https://de.cppreference.com/w/cpp/language/for)
- [C++ Referenz: While](https://de.cppreference.com/w/cpp/language/while)
- [C++ Referenz: Do-While](https://de.cppreference.com/w/cpp/language/do)
- [C++ Referenz: Range-For](https://de.cppreference.com/w/cpp/language/return)
- [C++ Referenz: Continue](https://de.cppreference.com/w/cpp/language/continue)
- [C++ Referenz: Break](https://de.cppreference.com/w/cpp/language/break)
- [C++ Referenz: Goto](https://de.cppreference.com/w/cpp/language/goto)
- [C++ Referenz: Return](https://de.cppreference.com/w/cpp/language/return)

## Folien

- [Flow](folien.pdf)

## Aufgaben

- [If](if.cpp)
- [For](for.cpp)
- [While](while.cpp)
- [Switch](switch.cpp)
- [Fizz Buzz](fizzbuzz.cpp)

