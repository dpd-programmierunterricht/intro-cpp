/// WHILE
///
/// Aufgabe
///
/// Implementiere die Funktion "teilbar_durch_zwei", die berechnet, wie oft eine Zahl
/// durch zwei teilbar ist.
///
/// Tipp: Mit dem Modulooperator (%) kannst du überprüfen, ob eine Zahl teilbar ist.
/// Nutze Google, um herauszufinden, wie du es benutzt.

#include <cassert>
#include <iostream>

int teilbar_durch_zwei(int zahl) {
    int teilbar = 0;

    /// TODO: überprüfe, ob die zahl immer noch durch 2 teilbar ist.
    while(???) {
        zahl = zahl /= 2;
        teilbar = teilbar + 1;
    }

    return teilbar;
}

int main() {
    assert(teilbar_durch_zwei(2) == 1);
    assert(teilbar_durch_zwei(4) == 2);
    assert(teilbar_durch_zwei(56) == 3);
}
