/// FIZZ BUZZ
///
/// Aufgabe
///
/// Implementiere die Funktion FizzBuzz so, dass diese drei Regeln gelten:
///
/// Wenn die Zahl durch 3 teilbar ist, returne "fizz".
/// Wenn die Zahl durch 5 teilbar ist, returne "buzz".
/// Wenn die Zahl durch 3 und 5 teilbar ist, returne "fizz buzz".
/// Ansonsten returne die zahl, konvertiert zu einem String.
///
/// Tipp: du kannst eine Zahl zu einem String konvertieren mit std::to_string.
/// Tipp: mit dem Modulo-Operator (%) kannst du testen, ob eine Zahl durch
///    eine andere Zahl teilbar ist.

#include <string>

std::string fizzbuzz(int zahl) {
    // TODO: implementiere diese Funktion!
    // Tipp: teste die vier Fälle in der richtigen Reihenfolge.
    return "todo"; // lösch mich!
}

int main() {
    assert(fizzbuzz(1) == "1");
    assert(fizzbuzz(2) == "2");
    assert(fizzbuzz(3) == "fizz");
    assert(fizzbuzz(4) == "4");
    assert(fizzbuzz(5) == "buzz");
    assert(fizzbuzz(6) == "fizz");
    assert(fizzbuzz(7) == "7");
    assert(fizzbuzz(15) == "fizz buzz");
}
