/// IF
///
/// Mit einer If-Abfrage kannst du zwei unterschiedliche Code-Pfade ausführen,
/// je nach dem ob die Bedingung wahr (true) oder falsch (false) ist.
///
/// Aufgaben:
///
/// 1. Implementiere die darf_bier_kaufen Funktion so, dass diese "return true;"
///    ausführt, wenn das Alter mindestens 16 ist, und ansonsten "return false;".
///
/// 2. Implementiere die darf_schnapps_kaufen Funktion so, dass diese "return true;"
///    ausführt, wenn das Alter mindestens 18 ist, und ansonsten "return false;".
///
///
/// Tipp: Mit dem <= Zeichen kannst du zwei Zahlen vergleichen.
#include <cassert>

bool darf_bier_kaufen(int alter) {
    // wenn unter 16: return false;
    // wenn 16 oder drüber: return true;
}

bool darf_schnapps_kaufen(int alter) {
    // genauso wie bei bier, aber mindestens 18.
}

int main() {
    assert(darf_bier_kaufen(1) == false);
    assert(darf_bier_kaufen(5) == false);
    assert(darf_bier_kaufen(10) == false);
    assert(darf_bier_kaufen(15) == false);
    assert(darf_bier_kaufen(16) == true);
    assert(darf_bier_kaufen(17) == true);
    assert(darf_bier_kaufen(18) == true);
    assert(darf_bier_kaufen(20) == true);
    assert(darf_bier_kaufen(25) == true);

    assert(darf_schnapps_kaufen(1) == false);
    assert(darf_schnapps_kaufen(5) == false);
    assert(darf_schnapps_kaufen(10) == false);
    assert(darf_schnapps_kaufen(15) == false);
    assert(darf_schnapps_kaufen(16) == false);
    assert(darf_schnapps_kaufen(17) == false);
    assert(darf_schnapps_kaufen(18) == true);
    assert(darf_schnapps_kaufen(20) == true);
    assert(darf_schnapps_kaufen(25) == true);

}
