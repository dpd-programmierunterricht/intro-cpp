/// SWITCH
///
/// Aufagbe:
///
/// Implementiere die "verstandkosten"-Funktion. Es gibt vier Paketarten:
/// PAKET_KLEIN, PAKET_GROSS, PAKET_REIFEN, PAKET_GEFAHRGUT. Dies sind die
/// Versandkosten von jeder Paketart:
///
/// KLEIN: 5€
/// GROSS: 9€
/// REIFEN: 12€
/// GEFAHRGUT: 19€.
///
/// Implementiere die Funktion so, dass diese für jede Paketart die Richtigen
/// Versandkosten zurückgibt (mit return).

#include <cassert>
#include <iostream>

enum paketart {
    PAKET_KLEIN,
    PAKET_GROSS,
    PAKET_REIFEN,
    PAKET_GEFAHRGUT,
};

float versandkosten(int paketart) {
    switch(paketart) {
        case PAKET_KLEIN:
            return ???;
            // TODO
        case PAKET_GROSS:
            // ...
    }
}

int main() {
    assert(versandkosten(PAKET_KLEIN) == 5);
    assert(versandkosten(PAKET_GROSS) == 9);
    assert(versandkosten(PAKET_REIFEN) == 12);
    assert(versandkosten(PAKET_GEFAHRGUT) == 19);
}
