# Summary

[Einführung](./prefix.md)
[Links](links.md)

# Konzepte

- [Variablen](./variablen.md)
    - [Aufgaben](./variablen/aufgaben.md)
- [Control-Flow](./flow.md)
- [Funktionen](./funktionen.md)
    - [Aufgaben](./funktionen/aufgaben.md)
- [Arrays](./arrays.md)
- [Strings](./strings.md)
- [Schleifen](schleifen.md)
- [Parameterübergabe]()

- [Templates]()

# Projekte

- [Badger2040](badger.md)
- [Kattis](kattis.md)
- [Tetris](tetris.md)
- [Sudoku]()
