# Arrays

## Einführung in Arrays

Ein Array ist eine Sammlung von Elementen, die denselben Datentyp haben und im
Speicher nacheinander abgelegt werden. Der Zugriff auf die Elemente erfolgt
über einen Index.

![Array](arrays.png)

Stell dir eine Array vor wie eine Liste von Elementen. Generell fangen
Programmierer bei Null an zu zählen, also werden die Elemente von Null an
nummeriert, und nicht von eins an.

## Statische Arrays

Statische Arrays haben eine feste Größe, die bereits zur Kompilierzeit bekannt
sein muss. Diese sind also wie eine Liste mit einer festen Länge:

<center>
<img src="form.svg" width="50%" />
</center>

In einem statischen Array sind also immer genau so viele Elemente, wie das
Array groß ist.

### Deklaration eines statischen Arrays

Die Deklaration eines statischen Arrays funktioniert ähnlich wie die
Deklaration eine Variable, nur musst du nach dem Namen der Variable noch die
Länge angeben. So kannst du Beispielsweise ein Array mit 10 Zahlen deklarieren:

```cpp
// Ein Array von 10 ganzen Zahlen
int meinArray[10];
```

Nach dieser Deklaration haben diese 10 `int` zufällige Werte. Es ist also
besser, diese gleich zu initialisieren.

### Initialisierung eines statischen Arrays

Du kannst ein statisches Array initialisieren, indem du eine Liste von Werten
in geschweiften Klammern angibst. Wenn du das tust, musst du die Länge auch
nicht angeben, denn die ergibt sich ja aus der Anzahl an Werten.

```cpp
// Ein Array von ganzen Zahlen mit Initialisierung
int zahlen[] = {1, 2, 3, 4, 5};
```

### Zugriff auf Array-Elemente

Um auf einen bestimmten Wert des Arrays zuzugreifen, kannst du in eckigen
Klammern angeben, auf welche Stelle du zugreifen willst. Achtung: die erste
Stelle hat den Index Null!

```cpp
// Zugriff auf das dritte Element des Arrays (Index zählt von 0 an)
int x = zahlen[2];
```

## Dynamische Arrays mit std::vector

[`std::vector`][] ist ein Container der Standardbibliothek, der sich wie ein
dynamisches Array verhält, d.h. seine Größe kann zur Laufzeit verändert werden.
Du kannst es ähnlich benutzen wie ein statisches Array.

Um es zu benuten, musst du den `vector`-Header importieren. Dafür reicht es, wenn
am Anfang deines Programmes folgende Zeile steht:

```cpp
#include <vector>
```

### Deklaration und Initialisierung eines std::vector

Da [`std::vector`] ein dynamisches Array ist, musst du bei diesem nicht die
Länge angeben.

```cpp
#include <vector>

// Ein dynamisches Array von ganzen Zahlen
std::vector<int> zahlen = {1, 2, 3, 4, 5};
```

### Zugriff auf Elemente eines std::vector

Der Zugriff auf die Elemente funktioniert hier genauso wie bei dem statischen
Array.

```cpp
// Zugriff wie bei einem statischen Array
int x = zahlen[2];
```

### Größe eines std::vector ändern

Der große Unterschied ist, dass du zur Laufzeit die Größe verändern kannst,
indem du Elemente hinzufügst oder entfernst. In der Dokumentation findest du
mehr Methoden, um mit [`std::vector`] zu arbeiten.

```cpp
// Fügt eine 6 am Ende hinzu
zahlen.push_back(6);

// Entfernt das letzte Element
zahlen.pop_back();
```

[`std::vector`]: https://en.cppreference.com/w/cpp/container/vector

## Aufgaben

### Aufgabe 1: Statisches Array deklarieren und Werte zuweisen

1. Deklariere ein statisches Array `temperaturen` mit 7 ganzen Zahlen.
2. Weise jeder Position im Array einen Wert zwischen 10 und 25 zu.

### Aufgabe 2: Werte eines Arrays ausgeben

Gegeben ist das folgende statische Array:

```cpp
int zahlen[] = {5, 10, 15, 20, 25, 30, 35};
```
1. Schreibe ein Programm, das alle Werte des Arrays in einer Schleife ausgibt.
2. Erweitere das Programm so, dass nur die Werte ausgegeben werden, die durch 5 teilbar sind.

### Aufgabe 3: Dynamisches Array verwenden

1. Deklariere und initialisiere ein `std::vector<int>` mit beliebigen ganzen Zahlen.
2. Füge einige Zahlen hinzu und entferne andere.
3. Gib alle Zahlen des `std::vector` aus.

### Aufgabe 4: Summe und Durchschnitt von Array-Elementen berechnen

1. Deklariere und initialisiere ein statisches oder dynamisches Array mit beliebigen ganzen Zahlen.
2. Berechne und gib die Summe sowie den Durchschnitt aller Zahlen im Array aus.

### Hinweise

- Achte darauf, dass der Index für den Zugriff auf Array-Elemente immer im gültigen Bereich liegt (0 bis Länge des Arrays - 1).
- Verwende eine Schleife, um über die Elemente eines Arrays zu iterieren.
- Denke daran, `#include <vector>` zu verwenden, wenn du `std::vector` benutzt.
- Bereite dich darauf vor, Fragen zu den Aufgaben in der nächsten Stunde zu beantworten.

## Abschluss
In dieser Stunde habt ihr gelernt, was statische und dynamische Arrays sind, wie man sie in C++ deklariert, initialisiert, darauf zugreift und manipuliert. Die Programmieraufgaben sollen euch helfen, das Gelernte praktisch anzuwenden und euer Verständnis zu vertiefen.
