# Einführung

In den nächsten Unterrichtseinheiten werden wir uns mit der Programmiersprache
C++ beschäftigen. 

C++ ist eine vielseitige Programmiersprache, die sich wie eine Werkzeugkiste
für Entwickler verhält. Sie ermöglicht die Erstellung von Computerprogrammen,
einschließlich Spielen und Anwendungen, und bietet die Möglichkeit,
benutzerdefinierte Datenarten zu erstellen, die spezielle Aufgaben erfüllen.
Mit der [C++-Standardbibliothek][stdlib] erhalten Entwickler bereits vorgefertigte
Bausteine, die in ihren Projekten verwendet werden können. Diese Sprache ist
bei Programmierneulingen und erfahrenen Entwicklern gleichermaßen beliebt, da
sie die Erstellung effizienter und anpassbarer Software für verschiedene
Anwendungen erleichtert.

> Wichtig ist: bei fast jeder Programmiersprache gibt es ähnliche Konzepte. Was
> du in diesen Einheiten lernst, kannst du in fast jeder anderen
> Programmiersprache ebenso anwenden.

Hier einmal ein sogenanntes [Hello World](helloworld)-Programm:

```cpp
#include <iostream>

int main() {
    std::cout << "Hello, World!" << std::endl;
}
```

In diesem Online-Buch findest du zu jedem Thema, welches wir besprechen,
Zusammenfassungen und Beispiele.

[stdlib]: https://de.cppreference.com/
[helloworld]: https://de.wikipedia.org/wiki/Hallo-Welt-Programm
