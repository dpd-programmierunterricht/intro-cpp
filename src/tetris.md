# Tetris

Tetris ist ein klassisches Puzzle-Videospiel, das ursprünglich vom russischen
Softwareentwickler Alexey Pajitnov im Jahr 1984 entwickelt wurde. Das
Spielprinzip ist einfach, aber herausfordernd: Verschieden geformte
Spielblöcke, bekannt als Tetrominos, fallen von oben herab.

<center>
<img src="tetrominoes.svg" width="50%" />

*Ein Bild mit den Tetrominos*

</center>

Der Spieler muss sie drehen und verschieben, um eine lückenlose horizontale
Linie am unteren Rand des Spielfeldes zu bilden. Sobald eine Linie komplett
ist, verschwindet sie, und der Spieler erhält Punkte. 


<center>
<img src="tetris.png" width="50%" />

*Das Spielfeld bei einem Tetris-Spiel*

</center>

Das Spiel wird schneller und schwieriger, je mehr Linien entfernt werden. Ziel
des Spiels ist es, so viele Punkte wie möglich zu sammeln, bevor die Tetrominos
den oberen Rand des Spielfelds erreichen und das Spiel endet. Tetris ist
bekannt für seine einfache, aber süchtig machende Spielmechanik und wurde auf
fast jeder Spieleplattform veröffentlicht, wodurch es zu einem der beliebtesten
und meistverkauften Videospiele aller Zeiten wurde.

## Aufgabe

Bei diesem Projekt haben wir das Ziel, Tetris selber zu implementieren. Dafür
müssen wir zuerst verstehen, wie das Spiel genau funktioniert. Dann können wir
uns überlegen, wie wir die Spielmechanik am besten repräsentieren können.

- Requirements
- Spielmechanik
- Implementierung

## Requirements

Siehe <https://tetr.io/>.

<img src="tetris.svg" />

### Spiellogik

- Jede Sekunde bewegen wir den aktuellen Spielstein ein Feld nach unten.
- Wenn der aktuelle Spielstein auf einer Oberflächte (z.B. dem Boden oder ein
  anderer Stein) landet, dann erstarrt dieser und kann nicht mehr bewegt
  werden.
- Wenn die oberste Ecke vom Stein beim Landen eine Höhe erreicht hat, die höher
  ist als das Limit, ist das Spiel verloren.
- Sobald ein Stein erstarrt ist, fängt der nächste Stein an, von oben zu
  fallen. Die Auswahl des nächsten Steines ist zufällig, nach einer festgelegten
  Wahrscheinlichkeit pro Spielstein.
- Sobald eine Zeile des Spielfeldes voll ist (keine freien Stellen),
  verschwindet diese und alle darüberliegenden Zeilen fallen herunter.
- Jedes Mal, wenn eine Zeile verschwindet, addieren wir 10 Punkte.
- Wenn ein Stein gedreht wird, wird der um seine Rotationsachse gedreht (diese
  ist oben markiert mit einem roten Punkt).
- **Optional**: Bei bestimmten Situationen gibt es Punkt-multiplikatoren, zum
  Beispiel:
    - Wenn mehrere Zeilen auf einmal aufgelöst werden.
    - Wenn bei aufeinanderfolgenden Spielsteinen jeweils Zeilen aufgelöst werden.

### Steuerung

- `← →`: Bewege Spielstein nach links oder rechts
- `↑`: Rotiere Spielstein im Uhrzeigersinn
- `↓`: Bewege Spielstein eine Zeile nach unten
- `Leertaste`: Bewege Spielstein nach ganz unten.
- **Optional**: Mit `S` kann man einen Spielstein aufbewahren.

## Spielmechanik

![Tetris states](tetris-states.png)

## Download

Das fertige Spiel kannst du hier herunterladen:

| Platform | Link |
| --- | --- |
| Linux (x86_64) | [tetris](https://dpd-programmierunterricht.gitlab.io/tetris/tetris) |


