# Schleifen

Mit eines der wichtigsten Werkzeuge in der Programmierung ist die Fähigkeit,
Abläufe beliebig oft zu widerholen. Schleifen sind dafür das Werkzeug, was
jeder Programmierer gut beherrschen sollte.

<center>
<img src="loop.png" width="200px" />
</center>

Du brauchst Schleifen also immer, wenn du etwas widerholen willst.

## Aufbau einer Schleife

Eine simple Endlosschleife sieht so aus:

```cpp
while(true) {
    // dieser code widerholt sich
    do_something();
}
```

Dann fügt man eine Variable hinzu, die zählt, wie oft die Schleife schon ausgeführt
wurde:

```cpp
int i = 0;
while(true) {
    // dieser code widerholt sich
    do_something();

    i = i + 1;
}
```

Dann fügt man eine Bedingung hinzu, die die Schleift abbricht, wenn eine gewünschte
Anzahl an Iterationen ausgeführt wurde:

```cpp
int i = 0;
while(i < 20) {
    // dieser code widerholt sich
    do_something();

    i = i + 1;
}
```

## Die `for`-Schleife

Dieses ist ein Pattern, welches so oft verwendet wird, dass man dafür einenen
eigenen Syntax hat: der `for`-Loop. Der Syntax einer `for`-Schleife wirkt auf
den ersten Blick verwirrend, macht aber mehr Sinn wenn man sich verinnerlicht,
dass die beiden folgenden Code-Beispiele äquivalent sind:

<div class="split">

```cpp
int i = 0;
while(i < 20) {
    // dieser code widerholt sich
    do_something();

    i = i + 1;
}
```

```cpp

for(int i = 0; i < 20; i = i + 1) {
    // dieser code wiederholt sich
    do_something();
}


```

</div>

## Aufgaben

Die Schleife in diesem Programm läuft 20 Mal, und gibt dabei alle Zahlen
von 0 bis 19 aus:

```cpp
#include <iostream>

int main() {
    for(int i = 0; i < 20; i = i + 1) {
        std::cout << "i = " << i << std::endl;
    }
}
```

Du kannst dieses Programm als Basis für die nächsten Antworten nutzen.  Nutze
bei jeder Antwort eine `for`-Schleife und ändere den Inhalt (das mit dem
`std::cout`) nicht, sondern nur den Kopf der Schleife (das mit dem `int i = 0;
i < 20; i++`). Nicht schummeln!

### Zähle in 2-er Schritten

Implementiere ein Programm, welches Zahlen von 0 bis 36 ausgibt, aber in 2-er
Schritten. Beispielsweise:

    0
    2
    4
    6
    …
    36

### Zähle Rückwärts

Kannst du eine Schleife implementieren, die Rückwärts zählt? Was sind dabei die
Schwierigkeiten?

### Summe der ersten N Zahlen

Kannst du ein Programm schreiben, welches die Summer aller zahlen von 0 bis *N*
berechnet? 

Beispielsweise, wenn *N* 6 ist, berechnet es die Summer der Zahlen 1, 2, 3, 4,
5, 6, diese ist 21.
