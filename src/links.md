# Links

Hier sind ein paar nützliche Links zusammengefasst. Diesen Abschnitt werde
ich aktiv überarbeiten. Wenn wir also ein neues Tool nutzen, werde ich das
hier einfügen.

> Beim Programmierenlernen gilt: das wichtigste Tool, welches du haben kannst,
> ist **Neugier** und **Mut**. Softwareenwicklung ist ein Feld, welches sich
> jederzeit weiterentwickelt. Niemand — kein Buch, kein Programmierkurs — kann
> dir alles vermitteln, was du wissen musst. Wir versuchen, die die
> Grundkonzepte beizubringen, damit du dir dann selber Wissen weiter aneignen
> kannst.

## C++-Referenz

Die wichtigste Resource zu C++ ist der [Standard][cppbuy]. Dies ist ein
Dokument, in dem die Sprache genau spezifiziert wird. Diesen kann man
[hier][std] kostenfrei lesen.
Allerdings ist der Standard ein *sehr* langes und schwer navigierbares Dokument.
Deswegen gibt es ein paar nützliche Webseiten, die Inhalte aus dem Standard
sinnvoll aufbereiten. Diese sind:

- [C++ Reference](https://en.cppreference.com/w/) ([deutsch](https://de.cppreference.com/w/))
- [C Plus Plus](https://cplusplus.com/)

Diese zwei Webseiten, vor allem die erstere, sind sehr hilfreich beim
Programmieren.  Es ist vollkommen normal, dass man hier erstmal einiges nicht
versteht. Bei Fragen könnt ihr gerne jederzeit eine E-Mail schreiben!

[cppbuy]: https://isocpp.org/std/the-standard
[std]: https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3797.pdf

## Online-Compiler

Eines der Schwierigkeiten bei C++ ist die Einrichtung einer
Entwicklungsumgebung.  Es gibt viele verschiedene Platformen (Windows, Linux,
macOS), die unterschiedliche, teils inkompatible Compiler verwenden. Mit diesen
Online-Compilern kannst du diese Einrichtung umgehen und einfach Code schreiben
und ausführen.

- [cpp.sh](https://cpp.sh/)
- [cplayground](https://cplayground.com/)

## Künstliche Intelligenz

Künstliche Intelligenz hat noch nicht die Macht übernommen, aber die versucht
es schon. Transformermodelle wie [Chat-GPT][chatgpt] können Fragen zu
Programmierthemen mittlerweile sehr gut beantworten.

- [Chat GPT][chatgpt]

Manchmal liegen diese Tools leider auch falsch, also vertraue ihnen nicht
blind, aber für schnelle Rückfragen, oder als Hilfe zur Fehlerbehebung sind
diese nützlich.

[chatgpt]: https://chat.openai.com
