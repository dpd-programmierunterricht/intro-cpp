# Strings

Was ist eingentlich ein *String*?  In dem Kapitel [Variablen](./variablen.md)
haben wir mit diesen schon einmal gearbeitet, aber nicht tiefergehend besprochen.
Heute schauen wir uns Strings mal genauer an.

Wahrscheinlich hast du schonmal einen String benutzt, indem du den `<string>`-Header
eingebunden hast:

```cpp
#include <string>
#include <iostream>

int main() {
    std::string name = "Thomas";

    std::cout << "Mein Name ist " << name << std::endl;
}
```

Daher weißt du schon, dass ein *String* generell eine Form von Text ist. Generell
schreibt man Strings immer zwischen doppelte Anführungszeichen:

```cpp
std::string text = "Dies ist ein String.";
```

Genauso, wie ein Wort aus Buchstaben besteht, besteht ein *String* aus *Zeichen*. Diese Zeichen 
werden aneinandergereiht, um den String zu ergeben. Der selbe String als eine Liste von Zeichen
ausgedrückt würde also so aussehen:

```cpp
std::string text = {'D', 'i', 'e', 's', ' ', 'i', 's', 't', ' ', 'e', 'i', 'n', ' ', 'S', 't', 'r', 'i', 'n', 'g', '.'};
```

Zeichen werden in C++ also mit einfachen Anführungszeichen aufgeschrieben:

-  `'a'` ist das Zeichen *a*, Datentyp `char`,
- `"a"` ist eine String, der nur aus dem Zeichen *a* besteht, Datentyp `std::string`.

## Zeichen

Welche Zeichen können in Strings vorkommen? 

- Buchstaben: `abcdefg...`
- Zahl: `0123456789`
- Leerzeichen: ` `
- Zeilenumbruch: `\n`
- Punktuation: `.,;:`
- Sonderzeichen: `[]{}()`

Meistens nutzt man Unicode mit der
[UTF-8](https://de.wikipedia.org/wiki/UTF-8)-Kodierung, um Zeichen
darzustellen. Diese ist kompatibel mit dem
[ASCII](https://de.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange)-Format,
welches früher eingesetzt wurde, aber nur Englische Zeichen darstellen kann
(keine Umlaute).

Hier ist einmal eine ASCII-Tabelle, diese zeigt dass jedem Buchstaben eine Zahl
zugewiesen wird.

![ASCII Tabelle](./ascii-table.svg)

Das bedeutet, dass diese drei Strings identisch sind. Der erste String ist
natürlich lesbarer, aber dies zeigt, dass Strings, genau wie alles am Computer,
im Endeffekt nur Zahlen sind.

```cpp
std::string text = "Dies ist ein String.";
std::string text = {'D', 'i', 'e', 's', ' ', 'i', 's', 't', ' ', 'e', 'i', 'n', ' ', 'S', 't', 'r', 'i', 'n', 'g', '.'};
std::string text = {68, 105, 101, 115, 32, 105, 115, 116, 32, 101, 105, 110, 32, 83, 116, 114, 105, 110, 103, 46};
```

> Tipp: Probier es mal aus! Schreibe ein Programm auf
> [cplayground](https://cplayground.com/?p=swan-axolotl-sloth), welches einen
> dieser Strings ausgibt und vergleiche die Ausgaben:
>
> ```cpp
> #include <string>
> #include <iostream>
>
> int main() {
>     std::string text = ???;
>     std::cout << text << std::endl;
> }
> ```

## Strings als Arrays von Zeichen

Einen [String](https://en.cppreference.com/w/cpp/string/basic_string) kannst du
dir also vorstellen wie ein Array von Zeichen. Genauso wie bei einem Array,
kannst du an einem String Zeichen anfügen mit der
[`push_back()`](https://en.cppreference.com/w/cpp/string/basic_string/push_back)-Methode:

```cpp
std::string text;
text.push_back('H');
text.push_back('a');
text.push_back('l');
text.push_back('l');
text.push_back('o');
```

Du kannst mehrere Strings zu einem verbinden mit dem `+`-Operator.

```cpp
std::string hallo = "Hallo";
std::string welt = "Welt";
std::string text = hallo + " " + welt;
```

Du kannst auf ein bestimmtes Zeichen in dem String zugreifen mit dem `[]`-Syntax.
Zum Beispiel kannst du so den Wert des 3. Zeichens ausgeben:

```cpp
std::string text = "abc";
std::cout << text[2] << std::endl;
```

## Aufgaben

### String übersetzen

Übersetze den String `"Mein Name ist $NAME"` in Dezimalwerte und gebe ihn so
aus. Dafür kannst du für jedes Zeichen in die ASCII-Tabelle oben schauen, und
den Wert aus der *Decimal*-Zeile kopieren.

Führe das Programm aus, und vergewisser dich, dass der String richtig angezeigt wird:

```cpp
#include <string>
#include <iostream>

int main() {
    std::string text = {77, 101, 105, ???};
    std::cout << text << std::endl;
    return 0;
}
```

### String zusammenführen

Schreibe eine Funktion, die einen Namen (als String) als Parameter besitzt und
also Rückgabewert einen neuen String ausgibt, der den Inhalt `"Hallo, {name}"`
hat, wobei `{name}` ersetzt werden soll durch den Namen, der übergeben wurde.

```cpp
#include <string>
#include <iostream>

std::string greet(std::string) {
    ???
    return ???;
}

int main() {
    std::cout << greet("Thomas") << std::endl;
}
```

### String ausgeben

Kannst du eine Funktion schreiben, die als Parameter einen Text (als String)
bekommt, und den Text ein einer Box ausgibt, die du mit `-+|` zeichnest? 

Beispielsweise, wenn der Parameter `Hallo` ist, soll die Ausgabe so aussehen:

```
+-------+
| Hallo |
+-------+
```

```cpp
#include <string>
#include <iostream>

void box(std::string) {
    /// ???
}

int main() {
    box("Hallo");
    box("Dies ist ein Test");
}
```

