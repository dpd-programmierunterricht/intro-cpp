# Aufgaben

## Add

Implementiere eine Funktion, die zwei Zahlen addiert.

```cpp
#include <cassert>

int addiere_zahlen(int a, int b) {
    return ???;
}

int main() {
    assert(addiere_zahlen(0, 0) == 0);
    assert(addiere_zahlen(0, 3) == 3);
    assert(addiere_zahlen(3, 0) == 3);
    assert(addiere_zahlen(3, 3) == 6);
}
```

[Hier][add.cpp] kannst du diese implementieren.

## Maximum

Implementiere eine Funktion, die die größte von drei Zahlen bestimmt.

```cpp
#include <cassert>

int maximale_zahl(int a, int b, int c) {
    return 0; // ???
}

int main() {
    assert(maximale_zahl(4, 3, 1) == 4);
    assert(maximale_zahl(1, 5, 2) == 5);
    assert(maximale_zahl(3, 2, 7) == 7);
}
```

[Hier][max.cpp] kannst du diese implementieren.

## Vokal oder nicht?

Schreibe eine Funktion, die für einen Buchstaben bestimmt, ob dieser ein
Vokal ist oder nicht. Wenn der Buchstabe ein Vokal ist, gebe `true` zurück.
Wenn dieser kein Vokal ist, gebe `false` zurück.

Es gibt mehrere Wege, diese Aufgabe zu lösen. Wenn du geschickt bist, kannst
du diese mit wenig Code lösen.

Tip: vergiss nicht, dass es auch Großbuchstaben gibt.

```cpp
#include <cassert>

bool ist_vokal(char c) {
    return false; // ???
}

int main() {
    assert(ist_vokal('a') == true);
    assert(ist_vokal('b') == false);
    assert(ist_vokal('o') == true);
    assert(ist_vokal('I') == true);
    assert(ist_vokal('z') == false);
}
```

[Hier][vokal.cpp] kannst du diese implementieren.

## Vokale zählen

Schreibe eine Funktion, die Vokale in einem String zählt. 

Tipp: überlege dir erst, wie du hier vorgehen könntest. Du darfst natürlich die
`ist_vokal` Funktion, die du vorhin geschrieben hast, hier auch verwenden!

```cpp
#include <cassert>
#include <string>

int vokal_zahl(std::string string) {
    return 0; // ???
}

int main() {
    assert(vokal_zahl("Hallo") == 2);
    assert(vokal_zahl("twttr") == 0);
    assert(vokal_zahl("aeiou") == 5);
}
```

[Hier][count.cpp] kannst du diese implementieren.

[add.cpp]: https://cplayground.com/?p=hare-frog-coyote
[max.cpp]: https://cplayground.com/?p=ibis-tapir-phil
[vokal.cpp]: https://cplayground.com/?p=ferret-porcupine-herring
[count.cpp]: https://cplayground.com/?p=finch-monarch-elephant
