# Funktionen

Eine Funktion beim Programmieren ist wie eine wiederverwendbare
Arbeitsanweisung, die bestimmte Aufgaben erledigt, wenn sie aufgerufen wird.
Sie hat einen Namen und kann Informationen verarbeiten oder zurückgeben, um den
Code in kleinere, leichter verständliche Teile aufzuteilen und die
Wiederverwendung von Code zu ermöglichen. 

Funktionen helfen dabei, verständlichen Code zu produzieren. Oftmals ist
es besser, viele kleine Funktionen zu verwenden als eine große.

## Syntax

Eine Funktion besteht aus einem *Namen*, einer *Liste von Parametern*, einen
*Returnwerttypen*, und dem *Inhalt*. Innerhalb der Funktion nutzt man ein
`return`-Statement, um einen Wert zurückzugeben.

![Funktionen Syntax](./funktionen/syntax.png)

Hier ist ein Beispiel einer Funktion mit dem Namen
`preis_mit_mehrwertsteuer`. Diese Funktion nimmt einen Parameter namens
`nettopreis` vom Typen `double`, und gibt einen Wert vom Typen `double` zurück.

```cpp
double preis_mit_mehrwertsteuer(double nettopreis) {
    double steuersatz = 0.19;
    double steuern = steuersatz * nettopreis;
    return nettopreis + steuern;
}
```

Diese Funktion kann nun als `preis_mit_mehrwertsteuer(..)` aufgerufen werden. 

```cpp
int main() {
    double bananen_preis = 2.99;
    double bananen_preis_mit_steuer = preis_mit_mehrwertsteuer(bananen_preis);
    std::cout << "Bananen kosten: " << bananen_preis_mit_steuer << std::endl;

    double avocado_preis = 5.27;
    double avocado_preis_mit_steuer = preis_mit_mehrwertsteuer(avocado_preis);
    std::cout << "Avocado kostet: " << avocado_preis_mit_steuer << std::endl;
}
```

Diesen Code kannst du [hier][example] ausprobieren.

[example]: https://cplayground.com/?p=bear-loris-lion

## Links

- [C++ Reference: Functions](https://en.cppreference.com/w/cpp/language/functions)
- [Funktionen.pdf](./funktionen/funktionen.pdf)

