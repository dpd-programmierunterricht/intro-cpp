# Aufgaben

## Integer

Integer bedeutet *ganze Zahl*. In einer Variable vom Typ Integer können
also ganze Zahlen gespeichert werden.

Aufgaben:

1. Repariere den Code so, dass er kompiliert und ausgeführt werden kann.
   Tipp: ersetze die Fragezeichen.

2. Was passiert, wenn du eine negative Zahl verwendest?

3. Was passiert, wenn du aus dem "int" ein "unsigned int" machst, und eine
   negative Zahl verwendest?
   Woran könnte das liegen?

4. Es gibt noch andere Integer-Typen, siehe die Tabelle bei
   <https://en.cppreference.com/w/cpp/language/types>: `short`, `signed char`, `long`.
   Was passiert, wenn du aus dem `int` ein `short` machst, und eine sehr große
   Zahl verwendest (größer als 1 Million)?  
   Kannst du dir denken, warum das passiert?  
   Kannst du versuchen herauszufinden, ab welchem Wert es passiert?

```cpp
#include <iostream>
#include <stdint.h>

int main() {
    /// TODO
    int alter = ???;

    std::cout << "Ich bin " << alter << " Jahre alt." << std::endl;
}
```

## Float


## Bool



## Char


## Vector


## String

