# Variablen

In der Programmierung ist eine Variable ein abstrakter Behälter für einen Wert.
Da C++ eine typisierte Sprache ist, haben Variablen einen festen Datentypen und
können nur Werte von diesem Typen beinhalten.



## Syntax

Variablen müssen **deklariert** werden, damit ihnen ein Wert **zugewiesen**
werden kann. Bei der *Deklaration* gibt man an, welchen Datentyp die Variable
hat. Bei der *Zuweisung* kann man der Variable dann beliebige Werte des
Datentypen zuweisen.

```cpp
// Deklaration
int meine_variable = 5;

// Zuweisung
meine_variable = 8;
```

## Links

Nützliche Links:

- [C++ Referenz: Grundtypen](https://de.cppreference.com/w/cpp/language/types)

