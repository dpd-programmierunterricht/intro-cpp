# Kattis

[Kattis][kattis] ist eine Platform, auf der man Programmier-Puzzles lösen kann. Jede Aufgabe
bei Kattis hat eine Beschreibung (auf Englisch), und die Möglichkeit, Code hochzuladen.
Kattis überprüft die Lösung. Wenn die Lösung korrekt ist, dann ist das Puzzle gelöst.

Heute nutzen wir Kattis, um ein paar Aufgaben zu lösen. Diese Aufgaben benutzen fast alles,
was wir in den letzten Stunden gelernt haben. 

Du kannst dir [hier][login] einen Account erstellen, am einfachsten nutzt du
*Login with Email*.

## Aufbau

The meisten Aufgaben bei Kattis haben eine ähnliche Struktur:
- du liest einen Input,
- du berechnest etwas,
- du gibst einen Output aus.

Wenn du eine Aufgabe hast, die als Input eine einzige Zahl erwartet, kannst du
den Input zum Beispiel so lesen:

```cpp
#include <iostream>

int main() {
    // input lesen
    int zahl;
    std::cin >> zahl;

    // ...
}
```

Wenn du einen solchen Input hast, mit einer Zahl *n* gefolgt von *n* Strings:

```
5
hallo
hallo
hallo
hallo
hallo
```

Dann kannst du diese so auslesen:

```cpp
#include <iostream>
#include <vector>
#include <string>

int main() {
    int anzahl;
    std::cin >> anzahl;

    std::vector<std::string> strings;
    for(int i = 0; i < anzahl; i++) {
        std::string wort;
        std::getline(std::cin, wort);
        strings.push_back(wort);
    }

    // ...
}
```

Mit diesen Templates solltest du eigentlich jede Aufgabe lösen können.

## Aufgaben

| Link | Aufgabe |
| --- | --- |
| [Hello World!](https://open.kattis.com/problems/hello) | Gibt den String `Hello, World!` aus. |
| [Which is greater](https://open.kattis.com/problems/whichisgreater) | Lies zwei Zahlen ein. Gib eine 1 aus wenn die erste Zahl größer ist als die zweite, ansonsten eine 0. |
| [Echo echo echo](https://open.kattis.com/problems/echoechoecho) | Lies ein Wort ein, gibt das eingelesene Wort drei Mal aus, separiert durch Leerzeichen. |
| [Add two numbers](https://open.kattis.com/problems/addtwonumbers) | Lies zwei Zahlen ein, gibt die Summer der Zahlen aus. |
| [Autori](https://open.kattis.com/problems/autori) | Lies einen String ein. Gib einen String aus, der nur die Großbuchstaben enthält. |
| [N sum](https://open.kattis.com/problems/nsum) | Lies eine Zahl `n` ein, gefolgt von *n* weiteren Zahlen. Gib die Summe der weiteren Zahlen aus. |
| [Digit swap](https://open.kattis.com/problems/digitswap) | Lies eine Zahl ein. Gib die gleiche Zahl aus, aber rückwärts (aus eine 14 wird eine 41). |
| [Quadrant](https://open.kattis.com/problems/quadrant) | Lies zwei Zahlen ein, behandle diese als `(x, y)` Koordinaten. Gib aus, in welchen Quadranten dieser Punkt wäre. |

[kattis]: https://open.kattis.com/
[login]: https://open.kattis.com/login
