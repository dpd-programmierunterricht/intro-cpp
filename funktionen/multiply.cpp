/// MULTIPLY
///
/// Implementiere eine Funktion, die zwei Zahlen multipliziert. Du darfst
/// nicht den eingebauten multiplikationsoperator (*) nutzen, sondern nur
/// Addition und For-Schleifen.

#include <cassert>

int multiply(int left, int right) {
    if(left == 0) {
        return 0;
    }

    return right + multiply(left - 1, right);
    /// TODO 1

    /// TODO 2
}

int main() {
    assert(multiply(0, 0) == 0);

    assert(multiply(0, 1) == 0);
    assert(multiply(1, 0) == 0);

    assert(multiply(6, 1) == 6);
    assert(multiply(1, 6) == 6);

    assert(multiply(3, 3) == 9);
    assert(multiply(2, 4) == 8);

    //assert(multiply(294239827497, 432482378987) == 8);
}
