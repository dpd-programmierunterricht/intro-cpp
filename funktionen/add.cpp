/// ADD
///
/// Implementiere die `addiere_zahlen` Funktion. D



#include <cassert>

int addiere_zahlen(int a, int b) {
    return a + b;
}

int main() {
    assert(addiere_zahlen(0, 0) == 0);
    assert(addiere_zahlen(0, 3) == 3);
    assert(addiere_zahlen(3, 0) == 3);
    assert(addiere_zahlen(3, 3) == 6);
}
