/// SORT
///
/// Implementiere einen Sortieralgorithmus.

#include <cassert>

void sort(int anzahl, int zahlen[]) {
    /// ???
}

int main() {
    int zahlen[] = { 2, 6, 1, 2, 0, 2, 42, 12, 14, 99 };
    int anzahl = 10;

    sort(anzahl, zahlen);

    assert(zahlen[0] == 0);
    assert(zahlen[1] == 1);
    assert(zahlen[2] == 2);
    assert(zahlen[3] == 2);
    assert(zahlen[4] == 2);
    assert(zahlen[5] == 6);
    assert(zahlen[6] == 12);
    assert(zahlen[7] == 14);
    assert(zahlen[8] == 42);
    assert(zahlen[9] == 99);
}
