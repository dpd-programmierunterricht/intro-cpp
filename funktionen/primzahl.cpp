/// PRIMZAHL
///
/// Der Code in dieser Datei funktioniert, er berechnet alle Primzahlen bis
/// `max_zahl`. Allerdings ist dieser Code komplett in der main-Methode
/// geschrieben und deswegen unübersichtlich. In dieser Aufgabe wirst du
/// den Code aufräumen und in geeignete Funktionen aufteilen.
///
/// Aufgaben:
///
/// 1. In der ersten If-Abfrage wird getestet, ob `kandidat` teilbar ist
///    durch `teiler`. Allerdings versteht man das nur, wenn man weiß, wie
///    der Modulo-Operator funktioniert. Erstelle eine neue Funktion namens
///    "ist_teilbar", die zwei Zahlen als Argument nimmt und einen boolean
///    zurückgibt, der `true` ist wenn die erste Zahl durch die zweite
///    teilbar ist.
///
/// 2. Die innere For-Schleife testet, ob der aktuelle Kandidat eine Primzahl
///    ist, indem diese alle möglichen Teiler zwischen 2 und dem Kandidat
///    austestet. Erstelle eine neue Funktion namens `ist_primzahl()`, die
///    nur eine Zahl als Input nimmt (den Kandidaten), und einen Wahrheitswert
///    zurückgibt, der wahr ist, wenn der Kandidat eine Primzahl ist.
///
/// 3. Die äußere For-Schleife berechnet eine Liste von Primzahlen in
///    dem vector "primzahlen". Mache aus dieser For-Schleife die funktion
///    "berechne_primzahlen", die als Input eine Zahl nimmt und einen vector
///    mit den Primzahlen zurückgibt.

#include <iostream>
#include <vector>

bool ist_teilbar(int zahl, int teiler);

bool ist_primzahl(int zahl);

std::vector<int> berechne_primzahlen(int max);

int main() {
    /// TODO: mache aus dieser For-Schleife die Funktion "berechne_primzahlen".
    int max_zahl = 100;
    std::vector<int> primzahlen;
    for(int kandidat = 2; kandidat < max_zahl; kandidat++) {
        bool ist_primzahl = true;

        /// TODO: mache aus dieser For-Schleife die Funktion "ist_primzahl".
        for(int teiler = 2; teiler < kandidat; teiler++) {
            /// TODO: mache aus dieser If-Abfrage die Funktion "ist_teilbar".
            if(kandidat % teiler == 0) {
                ist_primzahl = false;
            }
        }

        if(ist_primzahl) {
            primzahlen.push_back(kandidat);
        }
    }

    for(int &primzahl : primzahlen) {
        std::cout << "Die Zahl " << primzahl << " ist eine Primzahl." << std::endl;
    }
}
