# Variablen

In der Programmierung ist eine Variable ein abstrakter Behälter für einen Wert.
Da C++ eine typisierte Sprache ist, haben Variablen einen festen Datentypen und
können nur Variablen von diesem Typen beinhalten.

Nützliche Links:

- [C++ Referenz: Grundtypen](https://de.cppreference.com/w/cpp/language/types)

## Themen

- [Integer](integer.cpp)
- [Float](float.cpp)
- [Bool](bool.cpp)
- [Char](char.cpp)
- [Vector](vector.cpp)
- [String](string.cpp)
