/// BOOLEAN
///
/// Booleans sind Wahrheitswerte: also entweder "wahr" oder "falsch". Diese sind
/// nach George Boole benannt.
///
/// Aufgaben:
///
/// 1. Repariere den Code so, dass er kompiliert und ausgeführt werden kann.
///    Tipp: ersetze die Fragezeichen durch ein Wahrheitswert.
///
/// 2. Was passiert, wenn du einen anderen Wahrheitswert nutzt (also "wahr" statt
///    "falsch", oder umgekehrt)?

#include <iostream>

int main() {
    bool wetter_nice = ???;

    if(wetter_nice) {
        std::cout << "Das Wetter ist nice." << std::endl;
    } else {
        std::cout << "Das Wetter ist nicht nice." << std::endl;
    }
}
