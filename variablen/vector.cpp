/// VECTOR
///
/// Ein "vector" (auch array genannt) ist eine Reihe von Werten des selben
/// Types. Beispielsweise eine Liste von Zahlen, oder eine Liste von
/// Buchstaben.
///
/// Um den Typen `std::vector` zu nutzen, musst du es importieren mit
/// `#include <vector>`.
///
/// Aufgaben
///
/// 1. Repariere den Code so, dass er kompiliert und ausgeführt werden kann.
///    Tipp: importiere "vector" und ersetze die Fragezeichen.
///
/// 2. Was gibt es für andere Methoden, Elemente in den Vector einzufügen?
///
/// 3. Aktuell ist das ein Vector, der Werte vom Typen `int` beinhaletet. Kannst
///    du den Code so ändern, dass es ein Vector ist, der Typen vom Wert `short`
///    beinhaltet?
///    Kannst du den Code so ändern, dass es ein Vector ist, der Typen vom Wert
///    `unsigned int` beinhaltet?

#include <iostream>
/// TODO: import vector

int main() {
    std::vector<int> zahlen;

    /// TODO: füge 5 primzahlen hinzu
    zahlen.push_back(???);
    zahlen.push_back(???);
    zahlen.push_back(???);
    zahlen.push_back(???);
    zahlen.push_back(???);

    for(int &zahl : zahlen) {
        std::cout << "Eine Zahl ist " << zahl << std::endl;
    }
}
