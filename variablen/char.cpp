/// CHAR
///
/// "Char" steht für "Character", also ein Buchstabe.
///
/// Aufgaben:
///
/// 1. Repariere den Code so, dass er kompiliert und ausgeführt werden kann.
///    Tipp: ersetze die Fragezeichen.
///
/// 2. Was passiert, wenn du auf den Buchstaben eine 1 addierst?
///    Kannst du dir denken, warum das so ist?
///    Was passiert, wenn du andere Zahlen addierst?

#include <iostream>

int main() {
    // TODO: erster Buchstabe vom Namen
    char buchstabe = ???;

    std::cout << "Mein Name fängt mit '" << buchstabe << "' an." << std::endl;
}
