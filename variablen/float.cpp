/// FLOAT
///
/// "Float" bedeutet "floating-point number", also eine Gleitkommazahl.
/// Damit können also Zahlen mit Nachkommastellen gespeichert werden.
///
/// Aufgaben:
///
/// 1. Repariere den Code so, dass er kompiliert und ausgeführt werden kann.
///    Tipp: ersetze die Fragezeichen durch eine Zahl mit Nachkommastellen.
///
/// 2. Was passiert, wenn du eine negative Zahl verwendest?
///
/// 3. Was passiert, wenn du aus dem "float" ein "double" machst?
///    Kannst du dir denken, was der Unterschied ist?
///    Falls nicht, recherchiere was der Unterschied ist (nutze die Links).
///
/// 4. Kannst du eine Zahl finden, die zu groß ist für ein float?

#include <iostream>

int main() {
    // TODO
    float groesse = ???;

    std::cout << "Ich bin " << groesse << "m groß." << std::endl;
}
