/// STRING
///
/// "String" bedeutet "string of characters", also eine Anreihung von Buchstaben.
/// Man könnte es auch einfach als "Text" bezeichnen.
///
/// Strings werden in C++ durch den Typen `std::string` repräsentiert. Um diesen
/// zu nutzen, muss man den <string> header importieren.
///
/// Aufgaben:
///
/// 1. Repariere den Code so, dass er kompiliert und ausgeführt werden kann.
///    Tipp: ersetze die Fragezeichen.
///
/// 2. Kannst du in der C++-Referenz die Dokumentation von `std::string` finden?
///
/// 3. Kannst du nur den ersten Buchstaben des Stringes ausgeben?
///
/// 4. Kannst du einen Vektor von Strings anlegen, dort mehrere Namen einfügen
///    und für jeden Namen eine Begrüßung ausgeben?

#include <iostream>
// TODO: include string

int main() {
    /// TODO
    std::string name = ???;

    std::cout << "Hallo " << name << "!" << std::endl;
}
